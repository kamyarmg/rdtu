from conclusion.models import Rumor

a = Rumor.objects.all()
for i in a:
    if i.yes > 1 / 3 * (i.no + i.unknown):
        i.tag = 'Correct'
    else:
        i.tag = 'IDK'

    i.save()
