from semantic_text_similarity.models import ClinicalBertSimilarity

from conclusion.models import Rumor

c = ClinicalBertSimilarity(device='cpu')

r = Rumor.objects.all()
for item in r:
    for b in item.subject.users_text.all():
        c_rate = c.predict([(item.text, b.text)])[0]
        item.rate.create(rate=c_rate, user=b)
