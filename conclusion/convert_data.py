import glob
from itertools import chain
from xml.etree import ElementTree as ET

from conclusion.models import Subject, Rumor

dir_names = list(
    # chain.from_iterable([glob.glob('./sevEval2013_sci/{}/*.xml'.format(dir_name)) for dir_name in ['ud', 'ua', 'uq']]))
    chain.from_iterable([glob.glob('./beetls/test/{}/*.xml'.format(dir_name)) for dir_name in ['ua']]))
for q_file in dir_names:
    tree = ET.parse(q_file)
    root = tree.getroot()
    question = root.find('questionText')
    tuser_text = []
    subject = Subject.objects.create(text=question.text, s_id=root.get('id'), s_type='ua')
    for i in root.iter('referenceAnswer'):
        subject.users_text.create(text=i.text, t_id=i.get('id'))

    for std_ans in root.iter('studentAnswer'):
        Rumor.objects.create(
            text=std_ans.text,
            subject=subject,
            b_ans=std_ans.get('answerMatch'),
            r_id=std_ans.get('id'),
            accuracy=std_ans.get('accuracy')
        )
