from textwrap import shorten

from django.db import models


class TrustworthyUser(models.Model):
    text = models.TextField()
    t_id = models.CharField(max_length=32)

    def __str__(self):
        return shorten(self.text, 50)


class Subject(models.Model):
    s_id = models.CharField(max_length=32, null=True)
    text = models.TextField()
    users_text = models.ManyToManyField(to=TrustworthyUser)
    s_type = models.CharField(max_length=32, default='train')

    def __str__(self):
        return shorten(self.text, 50)


class BertRate(models.Model):
    rate = models.FloatField()
    user = models.ForeignKey(TrustworthyUser, on_delete=models.CASCADE)
    rate_tag = models.CharField(max_length=15)

    def __str__(self):
        return str(self.rate)


class Rumor(models.Model):
    r_id = models.CharField(max_length=32)
    text = models.TextField()
    b_ans = models.CharField(max_length=32, blank=True, null=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    rate = models.ManyToManyField(BertRate)
    tag = models.CharField(max_length=10, default='-')
    accuracy = models.CharField(max_length=32, default='')

    @property
    def txt(self):
        return shorten(self.text, 40)

    @property
    def bert_rate(self):
        return '-'.join([i.rate_tag for i in self.rate.all()])

    @property
    def yn(self):
        return 1 if self.yes > self.no else 0

    @property
    def yu(self):
        return 1 if self.yes > self.unknown else 0

    @property
    def nu(self):
        return 1 if self.no > self.unknown else 0

    @property
    def ny(self):
        return 1 if self.no > self.yes else 0

    @property
    def un(self):
        return 1 if self.unknown > self.no else 0

    @property
    def uy(self):
        return 1 if self.unknown > self.yes else 0






    @property
    def yes(self):
        c = self.rate.filter(rate_tag='YES').count()
        a = self.count
        return round(c / a * 100, 2)

    @property
    def y_n(self):
        c = self.rate.filter(rate_tag='NO').count()
        a = self.rate.filter(rate_tag='YES').count()
        return a - c

    @property
    def no(self):
        c = self.rate.filter(rate_tag='NO').count()
        a = self.count
        return round(c / a * 100, 2)

    @property
    def unknown(self):
        c = self.rate.filter(rate_tag='UNKNOWN').count()
        a = self.count
        return round(c / a * 100, 2)

    @property
    def count(self):
        return self.rate.count()

    def __str__(self):
        return self.tag
