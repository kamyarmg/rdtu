from django.contrib import admin

from conclusion.models import TrustworthyUser, Subject, Rumor, BertRate

from admin_numeric_filter.admin import RangeNumericFilter
from django.contrib import admin

from .models import *


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ('s_type', 'text')
    list_filter = ('s_type',)


@admin.register(Rumor)
class RumorAdmin(admin.ModelAdmin):
    list_display = ('txt', 'tag', 'accuracy', 'yes', 'no', 'unknown')
    list_filter = ('accuracy', 'tag', 'subject__s_type')
