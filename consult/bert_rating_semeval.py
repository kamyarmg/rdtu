from semantic_text_similarity.models import WebBertSimilarity, ClinicalBertSimilarity

from consult.models import StudentAnswerSemEval2013

std_ans = StudentAnswerSemEval2013.objects.all()
w = WebBertSimilarity(device='cpu')
c = ClinicalBertSimilarity(device='cpu')

for ans in std_ans:
    w_rate = w.predict([(ans.student_answer, ans.reference_answer.reference_answer)])[0]
    c_rate = c.predict([(ans.student_answer, ans.reference_answer.reference_answer)])[0]

    ans.web_bert = w_rate
    ans.clinical_bert = c_rate

    ans.save()


