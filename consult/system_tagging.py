from consult.models import StudentAnswerSemEval2013

student_answer = StudentAnswerSemEval2013.objects.filter(reference_answer__question_text__test_set__startswith='unseen')
for ans in student_answer:
    if ans.clinical_bert > 3:
        ans.clinical_tag = 'yes'
    elif ans.clinical_bert < 1.5:
        ans.clinical_tag = 'no'
    else:
        ans.clinical_tag = 'idn'
    ans.save()
