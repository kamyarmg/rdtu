import glob
from itertools import chain
from xml.etree import ElementTree as ET

from consult.models import *

dir_names = list(
    # chain.from_iterable([glob.glob('./sevEval2013_sci/{}/*.xml'.format(dir_name)) for dir_name in ['ud', 'ua', 'uq']]))
    chain.from_iterable([glob.glob('./sevEval2013_sci/{}/*.xml'.format(dir_name)) for dir_name in ['train']]))
for q_file in dir_names:
    tree = ET.parse(q_file)
    root = tree.getroot()
    question = root.find('questionText')
    question = QuestionSemEval2013.objects.create(
        question_text=question.text, test_set='train', question_id=root.get('id')
    )
    _answer = list(root.iter('referenceAnswer'))[0]
    answer = ReferenceAnswerSemEval2013.objects.create(
        question_text=question, reference_answer=_answer.text, reference_answer_id=_answer.get('id')
    )

    for std_ans in root.iter('studentAnswer'):
        StudentAnswerSemEval2013.objects.create(
            reference_answer=answer,
            student_answer=std_ans.text,
            student_answer_id=std_ans.get('id'),
            accuracy=std_ans.get('accuracy')
        )
