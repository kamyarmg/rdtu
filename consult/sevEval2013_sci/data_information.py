from consult.models import StudentAnswerSemEval2013

a = StudentAnswerSemEval2013.objects.filter(reference_answer__question_text__test_set='train')
b = {
    '1': (0, 0.5),
    '2': (0.5, 1.0),
    '3': (1.0, 1.5),
    '4': (1.5, 2.0),
    '5': (2.0, 2.5),
    '6': (2.5, 3.0),
    '7': (3.0, 3.5),
    '8': (3.5, 4.0),
    '9': (4.0, 4.5),
    '10': (4.5, 5.1),
}
c = {
    'contradictory': [],
    'correct': [],
    'irrelevant': [],
    'non_domain': [],
    'partially_correct_incomplete': []
}
for i in a:
    for j in b:
        if b[j][0] <= i.clinical_bert < b[j][1]:
            c[i.accuracy].append(j)
            continue

x = {}
dd = {}

for f in c:
    x[f] = {j: sum([1 for i in c[f] if i == j]) for j in b}

for f in c:
    dd[f] = {j: x[f][j] / sum([x[f][b] for b in x[f]]) * 100 for j in b if x[f][j] != 0}
    print(sum([x[f][b] for b in x[f]]))
