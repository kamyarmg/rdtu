from textwrap import shorten

from django.db import models


class QuestionSemEval2013(models.Model):
    test_set = models.CharField(max_length=32)
    question_id = models.CharField(max_length=32)
    question_text = models.TextField()

    def __str__(self):
        return shorten(self.question_text, 50)


class ReferenceAnswerSemEval2013(models.Model):
    question_text = models.ForeignKey(to=QuestionSemEval2013, on_delete=models.CASCADE)
    reference_answer = models.TextField()
    reference_answer_id = models.CharField(max_length=64)

    def __str__(self):
        return shorten(self.reference_answer, 50)


class StudentAnswerSemEval2013(models.Model):
    student_answer_id = models.CharField(max_length=32, blank=True, null=True)
    reference_answer = models.ForeignKey(to=ReferenceAnswerSemEval2013, on_delete=models.CASCADE)
    student_answer = models.TextField()
    accuracy = models.CharField(max_length=32)
    clinical_bert = models.FloatField(blank=True, null=True)
    web_bert = models.FloatField(blank=True, null=True)
    clinical_tag = models.CharField(max_length=32, blank=True)
    web_tag = models.CharField(max_length=32, blank=True)
    system_tag = models.CharField(max_length=32, blank=True)
    is_ok = models.BooleanField(default=False)

    def __str__(self):
        return shorten(self.student_answer, 50)

