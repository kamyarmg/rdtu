from admin_numeric_filter.admin import RangeNumericFilter
from django.contrib import admin

from .models import *


@admin.register(QuestionSemEval2013)
class QuestionSemEval2013Admin(admin.ModelAdmin):
    list_display = ('question_id', 'test_set', 'question_text')
    list_filter = ('test_set',)
    list_search = ('question_id', 'question_text')


@admin.register(ReferenceAnswerSemEval2013)
class ReferenceAnswerSemEval2013Admin(admin.ModelAdmin):
    list_display = ('reference_answer_id', 'question_text', 'reference_answer')
    list_search = ('question_text', 'reference_answer')


@admin.register(StudentAnswerSemEval2013)
class StudentAnswerSemEval2013Admin(admin.ModelAdmin):
    list_display = (
        'student_answer_id', 'reference_answer', 'student_answer', 'accuracy', 'clinical_bert', 'web_bert',
        'clinical_tag', 'web_tag'
    )
    list_filter = ('web_tag', 'clinical_tag', 'accuracy', 'reference_answer__question_text__test_set', 'is_ok',
                   ('web_bert', RangeNumericFilter), ('clinical_bert', RangeNumericFilter))
    list_search = ('student_answer', 'reference_answer')
