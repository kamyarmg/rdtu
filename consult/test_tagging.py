from consult.models import StudentAnswerSemEval2013

student_answer = StudentAnswerSemEval2013.objects.filter(reference_answer__question_text__test_set__startswith='unseen')
for ans in student_answer:
    if ans.clinical_tag == ans.accuracy:
        ans.is_ok = True
        ans.save()
